cmake_minimum_required(VERSION 2.8.0)

project(DX.GE.Suite)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

find_package(BoostSubmodule 1.57.0)
find_package(EigenSubmodule 3.2.4)
find_package(DirectX)

ADD_SUBDIRECTORY(Lib)
ADD_SUBDIRECTORY(Demos)
include(CTest)
ADD_SUBDIRECTORY(Tests)