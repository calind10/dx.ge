#ifndef DXGE_APPLICATION_H
#define DXGE_APPLICATION_H

#include <DX.GE.Exports.h>
#include <Declarations.h>
#include <string>

//fwd declaration
DECLARE_PTRS(IWindow)

class Application
{
public:
	//! \brief Creates the Application the will handle all setup and OS cummunication.
	//! \remark Only one application must exists per process.
	//! \throws std::exception if another Application exists for this process.
	DXGE_API Application();
	DXGE_API ~Application();

	//declare class moveonly
	Application(const Application&) = delete;
	Application& operator=(const Application&) = delete;

	//! \brief Tells the OS to create a new window.
	//! \remark Window will not be available for processing right away. Wait for windowCreated signal.
	//! \param width The width of the window (in pixels) with which it will be created.
	//! \param height The height of the window (in pixels) with which it will be created.
	//! \param title The initial window title.
	DXGE_API IWindowPtr createWindow(int width, int height, const std::string& title/*TODO: monitor, shared window context*/);
	//! \brief Runs the whole application. This should be used after all windows have been created and all connection made.
	//! Will not return until the application is asked to be closed.(last window closed)
	//! \return Exit code.
	DXGE_API int run();
	//! \brief Processes an OS event.
	//! \return true if applications can continue, false if application has been ask to close.
	DXGE_API bool tick();
};

#endif