#ifndef DXGE_WINDOW_H
#define DXGE_WINDOW_H

#include <DX.GE.Exports.h>
#include <Declarations.h>

#include <Eigen/Dense>
#include <boost/signals2.hpp>
#include <memory>

// pointer declaration
DECLARE_PTRS(IWindow)
DECLARE_PTRS(RenderingContext)
class RenderingContextDescription;

class IWindow
{
public:
	DXGE_API virtual ~IWindow() { }

	DXGE_API virtual void initializeRenderingContext(RenderingContextDescription const& desc) = 0;
	DXGE_API virtual RenderingContext* getRenderContext() = 0;

	using position_type = Eigen::Matrix<int, 1, 2>;
	using size_type = Eigen::Matrix<unsigned int, 1, 2>;
	//! \brief Close the window.
	DXGE_API virtual void close() = 0;

	//! \brief Change window visibility
	//! \param visible true to make the window visible, false to make is hidden.
	DXGE_API virtual void showWindow(bool visible = true) = 0;
	//! \brief Check if window is visible.
	//! \return true if window is visible, false if it's hidden.
	DXGE_API virtual bool isVisible() const = 0;

	//! \brief Chages the window minimization/maximization.
	//! \param minimize true if windows should be minimized to system tray, false if it should be restored from system tray.
	//! \remark Minimizing an already minimized windows or miximizing an already maximized window has no effect.
	DXGE_API virtual void minimizeWindow(bool minimize = true) = 0;
	//! \brief Checks if the window is minimized or restored.
	//! \return true if window is minimized, false otherwise.
	DXGE_API virtual bool isMinimized() const = 0;

	//! \brief Changes the window title.
	//! \param title new window title.
	DXGE_API virtual void setTitle(const std::string& title) = 0;
	//! \brief Gets the current title.
	DXGE_API virtual std::string getTitle() const = 0;

	//! \brief Tries to move the window to the foreground.
	DXGE_API virtual void setForeground() = 0;
	//! \brief Checks if the window is in the foreground.
	//! \return true The window is in foreground, false another window has focus.
	DXGE_API virtual bool isForeground() const = 0;

	//! \brief Changes the position of this window.
	//! \param position the new position of the top left corner of the window(in pixels).
	DXGE_API virtual void setPosition(const position_type& position) = 0;
	//! \brief Reads the position of the window.
	//! \return The position of the window(in pixels).
	DXGE_API virtual position_type getPosition() const = 0;

	//! \brief Changes the size of this window.
	//! \param size the new size of the window(in pixels).
	DXGE_API virtual void setSize(const size_type& size) = 0;
	//! \brief Reads the size of the window.
	//! \return The window size(in pixels).
	DXGE_API virtual size_type getSize() const = 0;

	template<typename... Args>
	inline boost::signals2::connection connectClosed(Args&&... args)
	{
		return m_closedSignal.connect(std::forward<Args>(args)...);
	}

	template<typename... Args>
	inline boost::signals2::connection connectMinimizeChanged(Args&&... args)
	{
		return m_minimizedChangedSignal.connect(std::forward<Args>(args)...);
	}

	template<typename... Args>
	inline boost::signals2::connection connectTitleChanged(Args&&... args)
	{
		return m_titleChangedSignal.connect(std::forward<Args>(args)...);
	}

	template<typename... Args>
	inline boost::signals2::connection connectFocusChanged(Args&&... args)
	{
		return m_focusChangedSignal.connect(std::forward<Args>(args)...);
	}

	template<typename... Args>
	inline boost::signals2::connection connectPositionChanged(Args&&... args)
	{
		return m_positionChangedSignal.connect(std::forward<Args>(args)...);
	}

	template<typename... Args>
	inline boost::signals2::connection connectSizeChanged(Args&&... args)
	{
		return m_sizeChangedSignal.connect(std::forward<Args>(args)...);
	}

protected:
	boost::signals2::signal<void()> m_closedSignal;
	boost::signals2::signal<void(bool)> m_minimizedChangedSignal;
	boost::signals2::signal<void(std::string)> m_titleChangedSignal;
	boost::signals2::signal<void(bool)> m_focusChangedSignal;
	boost::signals2::signal<void(position_type)> m_positionChangedSignal;
	boost::signals2::signal<void(size_type)> m_sizeChangedSignal;
};

#endif