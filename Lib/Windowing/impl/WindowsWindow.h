#ifndef DXGE_WINDOWSWINDOW_H
#define DXGE_WINDOWSWINDOW_H

#include <Windowing/IWindow.h>
#include <Rendering Context/RenderingContext.h>
#include <Declarations.h>
#include <windows.h>
#include <string>
#include <unordered_map>
#include <functional>

DECLARE_PTRS(WindowsWindow)
class WindowsWindow : public IWindow
{
public:
	WindowsWindow(LPCSTR className, HINSTANCE hInstance, int width, int height, const std::string& title);
	virtual ~WindowsWindow() { close(); }

	WindowsWindow(const WindowsWindow&) = delete;
	WindowsWindow& operator=(const WindowsWindow&) = delete;

	bool receiveWindowsMessage(UINT message, WPARAM wParam, LPARAM lParam);
	HWND getHandle() const;

	void close() override;
	void showWindow(bool showWindow) override;
	bool isVisible() const override;
	void minimizeWindow(bool minimized) override;
	bool isMinimized() const override;
	void setTitle(const std::string& title) override;
	std::string getTitle() const override;
	void setForeground() override;
	bool isForeground() const override;
	void setPosition(const position_type& position) override;
	position_type getPosition() const override;
	void setSize(const size_type& size) override;
	size_type getSize() const override;
	void initializeRenderingContext(RenderingContextDescription const& desc) override;
	RenderingContext* getRenderContext() override;

private:
	HWND m_hwnd;
	RenderingContextPtr m_pContext;

	// Called when WM_SIZE message has been received.
	void onWindowSize(WPARAM, LPARAM);
	// Called when WM_MOVE message has been received.
	void onWindowMove(WPARAM, LPARAM);
};

#endif