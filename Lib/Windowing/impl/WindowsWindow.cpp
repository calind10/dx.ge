#include <Windowing/impl/WindowsWindow.h>
#include <Rendering Context/impl/Direct3DRenderingContext.h>

namespace
{
	int extractWidth(const RECT& r)
	{
		return r.right - r.left;
	}

	int extractHeight(const RECT& r)
	{
		return r.bottom - r.top;
	}
}

WindowsWindow::WindowsWindow(LPCSTR className, HINSTANCE hInstance, int width, int height, const std::string& title)
{
	RECT wr{ 0, 0, width, height };
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);
	m_hwnd = CreateWindowEx(NULL
		, className
		, title.c_str()
		, WS_OVERLAPPEDWINDOW
		, CW_USEDEFAULT
		, CW_USEDEFAULT
		, ::extractWidth(wr)
		, ::extractHeight(wr)
		, NULL
		, NULL
		, hInstance
		, NULL);

	ShowWindow(m_hwnd, SW_SHOWNORMAL);
}

HWND WindowsWindow::getHandle() const
{
	return m_hwnd;
}

bool WindowsWindow::receiveWindowsMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
		close();
		return true;
	case WM_MOVE:
		onWindowMove(wParam, lParam);
		return true;
	case WM_SIZE:
		onWindowSize(wParam, lParam);
		return true;
	case WM_SETTEXT:
		m_titleChangedSignal(std::string(reinterpret_cast<const char*>(lParam)));
		return true;
	case WM_ACTIVATE:
		m_focusChangedSignal(LOWORD(wParam) != WA_INACTIVE);
		return true;
	default:
		break;
	}
	return false;
}

void WindowsWindow::onWindowSize(WPARAM wParam, LPARAM lParam)
{
	auto width = LOWORD(lParam);
	auto height = HIWORD(lParam);
	m_sizeChangedSignal(IWindow::size_type{ width, height });
	//detect minimized/restored. we are only interested in transition minimize->restored and backwards.
	if (wParam == SIZE_MINIMIZED)
		m_minimizedChangedSignal(true);
	else if (wParam == SIZE_RESTORED)
		m_minimizedChangedSignal(false);
}

void WindowsWindow::onWindowMove(WPARAM wParam, LPARAM lParam)
{
	auto x = LOWORD(lParam);
	auto y = HIWORD(lParam);
	RECT wr{ x, y, 0, 0 };
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);
	m_positionChangedSignal(IWindow::position_type{ wr.left, wr.top });
}

void WindowsWindow::close()
{
	if (m_hwnd != 0)
	{
		DestroyWindow(m_hwnd);
		m_hwnd = 0;
		m_closedSignal();
	}
}

void WindowsWindow::showWindow(bool show)
{
	ShowWindow(m_hwnd, show ? SW_SHOW : SW_HIDE);
}

bool WindowsWindow::isVisible() const
{
	return IsWindowVisible(m_hwnd) != FALSE;
}

void WindowsWindow::minimizeWindow(bool minimized)
{
	ShowWindow(m_hwnd, minimized ? SW_MINIMIZE : SW_NORMAL);
}

bool WindowsWindow::isMinimized() const
{
	return IsIconic(m_hwnd) != FALSE;
}

void WindowsWindow::setTitle(const std::string& title)
{
	SetWindowText(m_hwnd, title.c_str());
}

std::string WindowsWindow::getTitle() const
{
	auto titleSize = GetWindowTextLength(m_hwnd) + 1;
	std::vector<char> title(titleSize);
	GetWindowText(m_hwnd, title.data(), titleSize);
	return {title.begin(), title.begin() + titleSize - 1};
}

void WindowsWindow::setForeground()
{
	SetForegroundWindow(m_hwnd);
}

bool WindowsWindow::isForeground() const
{
	return GetForegroundWindow() == m_hwnd;
}

void WindowsWindow::setPosition(const position_type& position)
{
	const auto size = getSize();
	MoveWindow(m_hwnd, position[0], position[1], size[0], size[1], FALSE);
}

IWindow::position_type WindowsWindow::getPosition() const
{
	RECT rect;
	GetWindowRect(m_hwnd, &rect);
	return IWindow::position_type(rect.left, rect.top);
}

void WindowsWindow::setSize(const size_type& size)
{
	RECT rect{ 0, 0, size[0], size[1] };
	AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW, FALSE);
	SetWindowPos(m_hwnd, NULL, 0, 0, ::extractWidth(rect), ::extractHeight(rect), SWP_NOMOVE | SWP_NOZORDER);
}

IWindow::size_type WindowsWindow::getSize() const
{
	RECT rect;
	GetClientRect(m_hwnd, &rect);
	return IWindow::size_type(::extractWidth(rect), ::extractHeight(rect));
}

void WindowsWindow::initializeRenderingContext(RenderingContextDescription const& desc)
{
	auto pContext = std::make_shared<Direct3DRenderingContext>(m_hwnd, desc);
	pContext->initializeBackBuffers();
	m_pContext = pContext;
}

RenderingContext* WindowsWindow::getRenderContext()
{
	return m_pContext.get();
}