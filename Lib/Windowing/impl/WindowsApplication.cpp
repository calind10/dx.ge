#include <Windowing/Application.h>
#include <Windowing/impl/WindowsWindow.h>
#include <windows.h>
#include <map>

namespace
{
	// declare Windows specific singletons;
	WNDCLASSEX wndClass;
	HINSTANCE hInstance;
	LPCSTR wndClassName = "DX.GE.WndClass";
	bool applicationFinished = true;
	std::map<HWND, std::pair<WindowsWindowPtr, boost::signals2::scoped_connection>> KnownWindows;

	LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		auto it = KnownWindows.find(hWnd);
		bool handled = false;
		//let the known window handle it.
		if (it != KnownWindows.end())
		{
			bool handled = it->second.first->receiveWindowsMessage(message, wParam, lParam);
			// window destroyed...erase it
			if (message == WM_DESTROY)
			{
				KnownWindows.erase(it);
				// no windows left. just close the whole application.
				if (KnownWindows.empty())
				{
					applicationFinished = true;
					PostQuitMessage(0);
				}
			}
		}
		if (!handled)
		{
			// Handle any messages the switch statement didn't
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		else
		{
			return wParam;
		}
	}

	void initializeWndClass()
	{
		ZeroMemory(&wndClass, sizeof(wndClass));

		wndClass.cbSize = sizeof(wndClass);
		wndClass.style = CS_HREDRAW | CS_VREDRAW;
		wndClass.lpfnWndProc = WindowProc;
		wndClass.hInstance = hInstance;
		wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
		wndClass.lpszClassName = wndClassName;

		RegisterClassEx(&wndClass);
	}

	bool tick(MSG& msg)
	{
		bool messagesAvailable = PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != FALSE;
		if (messagesAvailable)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		return !applicationFinished && msg.message != WM_QUIT;
	}
}

Application::Application()
{
	if (!::applicationFinished)
	{
		throw std::exception("Application already started");
	}
	::applicationFinished = false;
	::hInstance = GetModuleHandle(nullptr);
	::initializeWndClass();
}

Application::~Application()
{
	::applicationFinished = true;
	auto windowMap = std::move(::KnownWindows);
	::KnownWindows.clear();
	for (auto& activeWindows : windowMap)
	{
		// block the closed connection first
		boost::signals2::shared_connection_block block(activeWindows.second.second);
		//close window
		activeWindows.second.first->close();
	}
	UnregisterClass(::wndClass.lpszClassName, ::hInstance);
}

IWindowPtr Application::createWindow(int width, int height, const std::string& title)
{
	auto window = std::make_shared<WindowsWindow>(::wndClassName, ::hInstance, width, height, title);
	auto handle = window->getHandle();
	auto windowCloseConnection = window->connectClosed(
		[handle]()
		{
			::KnownWindows.erase(handle);
		});
	::KnownWindows.emplace(window->getHandle(), std::make_pair(window, boost::signals2::scoped_connection{ windowCloseConnection }));

	return window;
}

int Application::run()
{
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (::tick(msg));
	return msg.wParam;
}

bool Application::tick()
{
	MSG msg;
	return ::tick(msg);
}