#ifndef DXGE_EXPORTS_H
#define DXGE_EXPORTS_H

#if defined(WIN32) && !defined(DXGE_STATIC_LINK)
#	ifdef DXGE_EXPORTS
#		define DXGE_API __declspec(dllexport)
#	else
#		define DXGE_API __declspec(dllimport)
#	endif
#else
#	define DXGE_API
#endif

#endif