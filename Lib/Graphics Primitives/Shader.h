#ifndef DXGE_SHADER_H
#define DXGE_SHADER_H

#include <DX.GE.Exports.h>
#include <Declarations.h>

#include <vector>
#include <fstream>
#include <string>
#include <cstdint>

DECLARE_PTRS(Shader)
DECLARE_PTRS(RenderingContext)

enum class ShaderType
{
	Color,
	Vertex,
	Geometry
};

class ShaderCompilationError : public std::runtime_error
{
public:
	ShaderCompilationError(const std::string& msg)
		: std::runtime_error(msg)
	{ }
};

class ShaderCompileArs
{
public:
	typedef std::pair<std::string, std::string> PreprocessorDefinition;
	std::string const& entryPoint() const { return m_entryPoint; }
	std::vector<std::string> const& includeDirs() const { return m_includeDirs; }
	std::vector<PreprocessorDefinition> const& getDefinitions() const { return m_definitions; }

	void setEntryPoint(std::string const& entryPoint){ m_entryPoint = entryPoint; }
	void setIncludeDirs(std::vector<std::string> const& includeDirs) { m_includeDirs = includeDirs; }
	void setDefinitions(std::vector<PreprocessorDefinition> const& definitions) { m_definitions = definitions; }

private:
	std::string m_entryPoint;
	std::vector<std::string> m_includeDirs;
	std::vector<PreprocessorDefinition> m_definitions;
};

class Shader
{
public:
	DXGE_API virtual ~Shader() { }
	DXGE_API ShaderType type() const { return m_type; }

	static DXGE_API ShaderPtr fromFile(ShaderType type, const std::string& filename, RenderingContextPtr pContext, const ShaderCompileArs& args = ShaderCompileArs());
	static DXGE_API ShaderPtr fromSourceCode(ShaderType type, const std::string& shaderSource, RenderingContextPtr pContext, const ShaderCompileArs& args = ShaderCompileArs());
	static DXGE_API ShaderPtr fromBlobFile(ShaderType type, const std::string& filename, RenderingContextPtr pContext, const ShaderCompileArs& args = ShaderCompileArs());
	static DXGE_API ShaderPtr fromCompiledBlob(ShaderType type, const std::vector<uint8_t>& blob, RenderingContextPtr pContext, const ShaderCompileArs& args = ShaderCompileArs());

protected:
	ShaderType m_type;
};

#endif