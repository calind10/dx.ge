#ifndef DXGE_VERTEX_BUFFER_H
#define DXGE_VERTEX_BUFFER_H

#include <DX.GE.Exports.h>

class VertexBuffer
{
public:
	DXGE_API virtual ~VertexBuffer() { }
};

#endif