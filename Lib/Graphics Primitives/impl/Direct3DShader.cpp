#include "Direct3DShader.h"
#include <D3Dcompiler.h>
#include <platform/Windows/ComPtr.hpp>
#include <unordered_map>

namespace
{
	std::unordered_map<ShaderType, std::string> ShaderTypeToHLSLType = 
	{
		{ ShaderType::Color, "ps" },
		{ ShaderType::Vertex, "vs" },
		{ ShaderType::Geometry, "gs" }
	};
}

ShaderPtr Shader::fromBlobFile(ShaderType type, const std::string& blobFile, RenderingContextPtr pContext, const ShaderCompileArs& args)
{

}

ShaderPtr Shader::fromCompiledBlob(ShaderType type, const std::vector<uint8_t>& blob, RenderingContextPtr pContext, const ShaderCompileArs& args)
{
}

ShaderPtr Shader::fromSourceCode(ShaderType type, const std::string& sourceCode, RenderingContextPtr pContext, const ShaderCompileArs& args)
{

}

ShaderPtr Shader::fromFile(ShaderType type, const std::string& sourceFile, RenderingContextPtr pContext, const ShaderCompileArs& args)
{
	const auto& defines = args.getDefinitions();
	std::vector<D3D_SHADER_MACRO> d3dDefines;
	d3dDefines.reserve(defines.size());
	std::transform(defines.cbegin(), defines.cend(), std::back_inserter(d3dDefines),
		[](decltype(*defines.cbegin()) item) -> D3D_SHADER_MACRO
		{
			D3D_SHADER_MACRO result;
			result.Name = item.first.c_str();
			result.Definition = item.second.c_str();
			return result; 
		} );
	ID3DInclude* pInclude = D3D_COMPILE_STANDARD_FILE_INCLUDE;
	if (!args.includeDirs().empty())
	{
		// todo: make your own include.
	}
	const std::string target = ::ShaderTypeToHLSLType.at(type);
	win32::COMPtr<ID3DBlob> pCode;
	win32::COMPtr<ID3D10Blob> pErrorMsg;
	UINT compileFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
	compileFlags |= D3DCOMPILE_DEBUG;
#endif
	const HRESULT result = D3DCompileFromFile(std::wstring{sourceFile.cbegin(), sourceFile.cend()}.c_str(),
		d3dDefines.empty() ? nullptr : d3dDefines.data(),
		pInclude,
		args.entryPoint().c_str(),
		target.c_str(),
		compileFlags,
		0,
		pCode.getPP(),
		pErrorMsg.getPP() );
	if (FAILED(result))
	{
		std::string errorMsg{ reinterpret_cast<const char*>(pErrorMsg->GetBufferPointer()), pErrorMsg->GetBufferSize() };
		throw ShaderCompilationError(errorMsg);
	}
	return Direct3DShader(type, pCode, pContext);
}