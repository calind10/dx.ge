#ifndef DXGE_RENDERING_TARGET_H
#define DXGE_RENDERING_TARGET_H

#include <DX.GE.Exports.h>
#include <Declarations.h>

DECLARE_PTRS(RenderingTarget)
DECLARE_PTRS(RenderingContext);

class RenderingTarget
{
public:
	DXGE_API virtual ~RenderingTarget() { }
	DXGE_API virtual void clearColor(const rgbaColor& clearColor) = 0;
	DXGE_API virtual void bind() = 0;
	DXGE_API virtual RenderingContextPtr getOwningContext() const = 0;
};


#endif