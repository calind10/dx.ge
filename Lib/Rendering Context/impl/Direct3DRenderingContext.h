#ifndef DXGE_DIRECT3D_RENDERING_CONTEXT_H
#define DXGE_DIRECT3D_RENDERING_CONTEXT_H

#include <Rendering Context/RenderingContext.h>
#include <Rendering Context/RenderingTarget.h>
#include <platform/Windows/ComPtr.hpp>

#include <windows.h>
#include <d3d11.h>
#include <d3d10.h>

#include <memory>
#include <vector>

class RenderingContextDescription;

class Direct3DRenderingContext : public RenderingContext, public std::enable_shared_from_this<Direct3DRenderingContext>
{
public:
	Direct3DRenderingContext(HWND handle, RenderingContextDescription const& desc);
	~Direct3DRenderingContext();

	RenderingTargetPtr getBackRenderTarget(unsigned int targetNo) const override;
	void presetBackBuffer() override;

	win32::COMPtr<IDXGISwapChain> swapChain() const { return m_pSwapChain; }
	win32::COMPtr<ID3D11Device> device() const { return m_pDevice; }
	win32::COMPtr<ID3D11DeviceContext> deviceContext() const { return m_pDeviceContext; }
	void initializeBackBuffers();

private:
	win32::COMPtr<IDXGISwapChain> m_pSwapChain;
	win32::COMPtr<ID3D11Device> m_pDevice;
	win32::COMPtr<ID3D11DeviceContext> m_pDeviceContext;
	std::vector<RenderingTargetPtr> m_renderingBackBuffers;
};

#endif