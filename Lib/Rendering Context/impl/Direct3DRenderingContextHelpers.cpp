#include <Rendering Context/impl/Direct3DRenderingContextHelpers.h>
#include <unordered_map>

namespace
{
	const std::unordered_map<PixelFormat, DXGI_FORMAT> PixelFormatMapping =
	{
		{ PixelFormat::R32G32B32A32_FLOAT, DXGI_FORMAT_R32G32B32A32_FLOAT },
		{ PixelFormat::R32G32B32A32_UINT, DXGI_FORMAT_R32G32B32A32_UINT },
		{ PixelFormat::R32G32B32A32_SINT, DXGI_FORMAT_R32G32B32A32_SINT },
		{ PixelFormat::R32G32B32_FLOAT, DXGI_FORMAT_R32G32B32_FLOAT },
		{ PixelFormat::R32G32B32_UINT, DXGI_FORMAT_R32G32B32_UINT },
		{ PixelFormat::R32G32B32_SINT, DXGI_FORMAT_R32G32B32_SINT },
		{ PixelFormat::R16G16B16A16_FLOAT, DXGI_FORMAT_R16G16B16A16_FLOAT },
		{ PixelFormat::R16G16B16A16_UNORM, DXGI_FORMAT_R16G16B16A16_UNORM },
		{ PixelFormat::R16G16B16A16_UINT, DXGI_FORMAT_R16G16B16A16_UINT },
		{ PixelFormat::R16G16B16A16_SNORM, DXGI_FORMAT_R16G16B16A16_SNORM },
		{ PixelFormat::R16G16B16A16_SINT, DXGI_FORMAT_R16G16B16A16_SINT },
		{ PixelFormat::R32G32_FLOAT, DXGI_FORMAT_R32G32_FLOAT },
		{ PixelFormat::R32G32_UINT, DXGI_FORMAT_R32G32_UINT },
		{ PixelFormat::R32G32_SINT, DXGI_FORMAT_R32G32_SINT },
		{ PixelFormat::R8G8B8A8_UNORM, DXGI_FORMAT_R8G8B8A8_UNORM },
		{ PixelFormat::R8G8B8A8_UNORM_SRGB, DXGI_FORMAT_R8G8B8A8_UNORM_SRGB },
		{ PixelFormat::R8G8B8A8_UINT, DXGI_FORMAT_R8G8B8A8_UINT },
		{ PixelFormat::R8G8B8A8_SNORM, DXGI_FORMAT_R8G8B8A8_SNORM },
		{ PixelFormat::R8G8B8A8_SINT, DXGI_FORMAT_R8G8B8A8_SINT }
	};

	const std::unordered_map<Usage, DXGI_USAGE> UsageMapping = 
	{
		{ Usage::BackBuffer, DXGI_USAGE_BACK_BUFFER },
		{ Usage::ReadOnly, DXGI_USAGE_READ_ONLY },
		{ Usage::RenderTarget, DXGI_USAGE_RENDER_TARGET_OUTPUT }
	};

	const std::unordered_map<BindBehavior, DXGI_SWAP_EFFECT> BindBehaviorMapping =
	{
		{ BindBehavior::Discard, DXGI_SWAP_EFFECT_DISCARD },
		{ BindBehavior::Keep, DXGI_SWAP_EFFECT_SEQUENTIAL }
	};
}

namespace dx
{
	DXGI_FORMAT fromPixelFormat(PixelFormat format)
	{
		return ::PixelFormatMapping.at(format);
	}

	DXGI_USAGE fromUsage(Usage usage)
	{
		return ::UsageMapping.at(usage);
	}

	DXGI_SWAP_EFFECT fromBindBehavior(BindBehavior behavior)
	{
		return ::BindBehaviorMapping.at(behavior);
	}

	void fillFromRenderingContextDescription(RenderingContextDescription const& from, DXGI_SWAP_CHAIN_DESC& to)
	{
		fillFromBufferDescription(from.getBufferDescription(), to.BufferDesc);
		fillFromSampling(from.getSampling(), to.SampleDesc);
		to.BufferUsage = fromUsage(from.getUsage());
		to.BufferCount = from.getBufferCount();
		to.Windowed = from.isFullScreen() ? FALSE : TRUE;
		to.SwapEffect = fromBindBehavior(from.getBindBehavior());
		to.Flags = 0;
	}

	void fillFromSampling(Sampling const& from, DXGI_SAMPLE_DESC& to)
	{
		to.Count = from.count();
		to.Quality = from.quality();
	}

	void fillFromBufferDescription(BufferDescription const& from, DXGI_MODE_DESC& to)
	{
		to.Width = from.getWidth();
		to.Height = from.getHeight();
		if (from.getVsync())
		{
			to.RefreshRate.Numerator = 1;
			to.RefreshRate.Denominator = from.getRefreshRate();
		}
		else
		{
			to.RefreshRate.Numerator = 0;
			to.RefreshRate.Denominator = 1;
		}
		to.Format = fromPixelFormat(from.getPixelFormat());
		to.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		to.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	}
}