#include "Direct3DRenderingContext.h"
#include <Rendering Context/impl/Direct3DRenderingTarget.h>
#include <Rendering Context/impl/Direct3DRenderingContextHelpers.h>
#include <Rendering Context/RenderingContextDescription.h>
#include <assert.h>
#include <memory>

Direct3DRenderingContext::Direct3DRenderingContext(HWND hwnd, RenderingContextDescription const& desc)
{
	// create a struct to hold information about the swap chain
	DXGI_SWAP_CHAIN_DESC scd;

	dx::fillFromRenderingContextDescription(desc, scd);
	scd.OutputWindow = hwnd;
	// create a device, device context and swap chain using the information in the scd struct
	D3D11CreateDeviceAndSwapChain(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		NULL,
		NULL,
		NULL,
		D3D11_SDK_VERSION,
		&scd,
		m_pSwapChain.getPP(),
		m_pDevice.getPP(),
		NULL,
		m_pDeviceContext.getPP());
}

Direct3DRenderingContext::~Direct3DRenderingContext()
{ }

RenderingTargetPtr Direct3DRenderingContext::getBackRenderTarget(unsigned int targetNo) const
{
	assert(m_pSwapChain);

	if (targetNo > m_renderingBackBuffers.size())
		throw std::exception("Target number larger than available buffers");
	return m_renderingBackBuffers[targetNo];
}

void Direct3DRenderingContext::presetBackBuffer()
{
	m_pSwapChain->Present(0, 0);
}

void Direct3DRenderingContext::initializeBackBuffers()
{
	DXGI_SWAP_CHAIN_DESC desc;
	m_pSwapChain->GetDesc(&desc);
	for (size_t i = 0; i < desc.BufferCount; ++i)
	{
		win32::COMPtr<ID3D11Texture2D> pBuffer;
		m_pSwapChain->GetBuffer(i, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(pBuffer.getPP()));
		win32::COMPtr<ID3D11RenderTargetView> pBufferView;
		m_pDevice->CreateRenderTargetView(pBuffer.get(), nullptr, pBufferView.getPP());

		m_renderingBackBuffers.emplace_back(std::make_shared<Direct3DRenderingTarget>(pBufferView, shared_from_this()));
	}
}