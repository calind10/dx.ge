#include "Direct3DRenderingTarget.h"
#include "Direct3DRenderingContext.h"

Direct3DRenderingTarget::Direct3DRenderingTarget(win32::COMPtr<ID3D11RenderTargetView> targetView, Direct3DRenderingContextPtr pContext)
	: m_pTargetView(targetView)
	, m_pContext(pContext)
{ }

Direct3DRenderingTarget::~Direct3DRenderingTarget()
{ }

void Direct3DRenderingTarget::clearColor(const rgbaColor& color)
{
	assert(!m_pContext.expired());
	m_pContext.lock()->deviceContext()->ClearRenderTargetView(m_pTargetView.get(), color.data());
}

void Direct3DRenderingTarget::bind()
{
	assert(!m_pContext.expired());
	m_pContext.lock()->deviceContext()->OMSetRenderTargets(1, m_pTargetView.getPP(), nullptr);
}

RenderingContextPtr Direct3DRenderingTarget::getOwningContext() const
{
	return m_pContext.lock();
}