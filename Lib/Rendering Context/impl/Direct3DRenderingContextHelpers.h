#ifndef DXGE_RENDERING_CONTEXT_HELPERS_H
#define DXGE_RENDERING_CONTEXT_HELPERS_H

#include <Rendering Context/RenderingContextDescription.h>
#include <d3d11.h>

namespace dx
{
	DXGI_FORMAT fromPixelFormat(PixelFormat format);
	DXGI_USAGE fromUsage(Usage usage);
	DXGI_SWAP_EFFECT fromBindBehavior(BindBehavior behavior);

	void fillFromRenderingContextDescription(RenderingContextDescription const& from, DXGI_SWAP_CHAIN_DESC& to);
	void fillFromSampling(Sampling const& from, DXGI_SAMPLE_DESC& to);
	void fillFromBufferDescription(BufferDescription const& from, DXGI_MODE_DESC& to);
}

#endif