#ifndef DXGE_DIRECT3D_RENDERING_TARGET_H
#define DXGE_DIRECT3D_RENDERING_TARGET_H

#include <Rendering Context/RenderingTarget.h>
#include <platform/Windows/ComPtr.hpp>
#include <d3d11.h>

DECLARE_PTRS(Direct3DRenderingContext)

class Direct3DRenderingTarget : public RenderingTarget
{
public:
	Direct3DRenderingTarget(win32::COMPtr<ID3D11RenderTargetView> targetView, Direct3DRenderingContextPtr pContext);
	~Direct3DRenderingTarget();

	void clearColor(const rgbaColor& color) override;
	void bind() override;
	RenderingContextPtr getOwningContext() const override;

private:
	win32::COMPtr<ID3D11RenderTargetView> m_pTargetView;
	Direct3DRenderingContextWPtr m_pContext;
};

#endif