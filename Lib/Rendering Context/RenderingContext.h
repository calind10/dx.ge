#ifndef DXGE_RENDERING_CONTEXT_H
#define DXGE_RENDERING_CONTEXT_H

#include <Declarations.h>
#include <DX.GE.Exports.h>

DECLARE_PTRS(RenderingTarget)
DECLARE_PTRS(IndexBuffer)
DECLARE_PTRS(VertexBuffer)

class RenderingContext
{
public:
	DXGE_API virtual ~RenderingContext() { }
	DXGE_API virtual RenderingTargetPtr getBackRenderTarget(unsigned int targetNo) const = 0;
	DXGE_API virtual void presetBackBuffer() = 0;

	//DXGE_API virtual IndexBufferPtr createIndexBuffer() = 0;
	//DXGE_API virtual VertexBufferPtr createVertexBuffer() = 0;
};

#endif 