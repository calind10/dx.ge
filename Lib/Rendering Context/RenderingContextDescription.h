#ifndef DXGE_RENDERING_CONTEXT_DESCRIPTION_H
#define DXGE_RENDERING_CONTEXT_DESCRIPTION_H

enum class PixelFormat
{
	R32G32B32A32_FLOAT,
	R32G32B32A32_UINT,
	R32G32B32A32_SINT,
	R32G32B32_FLOAT,
	R32G32B32_UINT,
	R32G32B32_SINT,
	R16G16B16A16_FLOAT,
	R16G16B16A16_UNORM,
	R16G16B16A16_UINT,
	R16G16B16A16_SNORM,
	R16G16B16A16_SINT,
	R32G32_FLOAT,
	R32G32_UINT,
	R32G32_SINT,
	R8G8B8A8_UNORM,
	R8G8B8A8_UNORM_SRGB,
	R8G8B8A8_UINT,
	R8G8B8A8_SNORM,
	R8G8B8A8_SINT,
};

class BufferDescription
{
public:
	inline unsigned int getWidth() const { return m_width; }
	inline void setWidth(unsigned int width) { m_width = width; }
	
	inline unsigned int getHeight() const { return m_height; }
	inline void setHeight(unsigned int height) { m_height = height; }

	inline unsigned int getRefreshRate() const { return m_refreshRate; }
	inline void setRefreshRate(unsigned int refreshRate) { m_refreshRate = refreshRate; }

	inline PixelFormat getPixelFormat() const { return m_pixelFormat; }
	inline void setPixelFormat(PixelFormat format) { m_pixelFormat = format; }

	inline bool getVsync() const { return m_vsync; }
	inline void setVsync(bool vsync) { m_vsync = vsync; }

	//! \brief Initialize the buffer description with default values.
	BufferDescription()
		: m_width(800)
		, m_height(680)
		, m_refreshRate(60)
		, m_pixelFormat(PixelFormat::R8G8B8A8_UNORM_SRGB)
		, m_vsync(true)
	{ }
private:
	unsigned int m_width;
	unsigned int m_height;
	unsigned int m_refreshRate;
	bool m_vsync;
	PixelFormat m_pixelFormat;
};

class Sampling
{
public:
	inline unsigned int count() const { return m_samplingCount; }
	inline void setSamplingCount(unsigned int count) { m_samplingCount = count; }

	inline unsigned int quality() const { return m_quality; }
	inline void setQuality(unsigned int quality) { m_quality = quality; }

	Sampling()
		: m_samplingCount(2)
		, m_quality(1)
	{ }
private:
	unsigned int m_samplingCount;
	unsigned int m_quality;
};

enum class BindBehavior
{
	Discard,
	Keep
};

enum class Usage
{
	BackBuffer,
	ReadOnly,
	RenderTarget
};

class RenderingContextDescription
{
public:
	inline unsigned int getBufferCount() const { return m_bufferCount; }
	inline void setBufferCount(unsigned int count) { m_bufferCount = count; }

	inline bool isFullScreen() const { return m_isFullScreen; }
	inline void setIsFullScreen(bool isFullScreen) { m_isFullScreen = isFullScreen; }

	inline BindBehavior getBindBehavior() const { return m_bindBehavior; }
	inline void setBindBehavior(BindBehavior behavior) { m_bindBehavior = behavior; }

	inline Usage getUsage() const { return m_usage; }
	inline void setUsage(Usage usage) { m_usage = usage; }

	inline BufferDescription const& getBufferDescription() const { return m_bufferDesc; }
	inline void setBufferDescription(BufferDescription const& bufferDesc) { m_bufferDesc = bufferDesc; }

	inline Sampling const& getSampling() const { return m_sampling; }
	inline void setSampling(Sampling const& sampling) { m_sampling = sampling; }

	//! \brief Init RenderingContextDescription with predefined initial values.
	RenderingContextDescription()
		: m_bufferCount(2)
		, m_isFullScreen(false)
		, m_bindBehavior(BindBehavior::Discard)
		, m_usage(Usage::RenderTarget)
	{ }
private:
	unsigned int m_bufferCount;
	bool m_isFullScreen;
	BindBehavior m_bindBehavior;
	Usage m_usage;
	BufferDescription m_bufferDesc;
	Sampling m_sampling;
};

#endif //DXGE_RENDERING_CONTEXT_DESCRIPTION_H