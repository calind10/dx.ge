#ifndef DXGE_DECLARATIONS_H
#define DXGE_DECLARATIONS_H

#include <memory>
#include <Eigen/Dense>

#define DECLARE_PTRS(Class)\
class Class;\
using Class##Ptr = std::shared_ptr<Class>;\
using Class##WPtr = std::weak_ptr<Class>;\
using Class##Ref = std::unique_ptr<Class>;

using rgbaColor = Eigen::Matrix < float, 1, 4 > ;

#endif