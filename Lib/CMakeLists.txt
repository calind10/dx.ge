set(PROJECT_NAME "DX.GE")

project(${PROJECT_NAME})

include_directories(
	${CMAKE_CURRENT_SOURCE_DIR}
	${Boost_INCLUDE_DIR}
	${Eigen_INCLUDE_DIR}
)

link_directories(
	${Boost_LIB_DIR}
)

set(GRAPHICS_PRIMITIVES_API
	"Graphics Primitives/Shader.h"
	"Graphics Primitives/IndexBuffer.h"
	"Graphics Primitives/VertexBuffer.h"
)

set(GRAPHICS_PRIMITIVES_DIRECTX_SOURCES
	"Graphics Primitives/impl/Direct3DShader.cpp"
	"Graphics Primitives/impl/Direct3DIndexBuffer.cpp"
	"Graphics Primitives/impl/Direct3DVertexBuffer.cpp"
	"Graphics Primitives/impl/Direct3DShader.h"
	"Graphics Primitives/impl/Direct3DIndexBuffer.h"
	"Graphics Primitives/impl/Direct3DVertexBuffer.h"
)

set(WINDOW_MANAGEMENT_API
	"Windowing/Application.h"
	"Windowing/IWindow.h"
)

set(RENDERING_CONTEXT_API
	"Rendering Context/RenderingContext.h"
	"Rendering Context/RenderingTarget.h"
	"Rendering Context/RenderingContextDescription.h"
)

set(RENDERING_CONTEXT_DIRECTX_SOURCES
	"Rendering Context/impl/Direct3DRenderingContext.h"
	"Rendering Context/impl/Direct3DRenderingContext.cpp"
	"Rendering Context/impl/Direct3DRenderingTarget.h"
	"Rendering Context/impl/Direct3DRenderingTarget.cpp"
	"Rendering Context/impl/Direct3DRenderingContextHelpers.cpp"
	"Rendering Context/impl/Direct3DRenderingContextHelpers.h"
)

set(WINDOW_MANAGEMENT_WINDOWS_SOURCE
	"Windowing/impl/WindowsApplication.cpp"
	"Windowing/impl/WindowsWindow.cpp"
	"Windowing/impl/WindowsWindow.h"
)

if(WIN32)
	set(PLATFORM_SPECIFIC_HEADERS
		"platform/Windows/ComPtr.hpp"
	)
else()
	message(FATAL_ERROR "Non-windows not supported yet")
endif()

set(DXGE_PUBLIC_HEADERS
	DX.GE.Exports.h
	Declarations.h
	${WINDOW_MANAGEMENT_API}
	${GRAPHICS_PRIMITIVES_API}
	${RENDERING_CONTEXT_API}
	${PLATFORM_SPECIFIC_HEADERS}
)

if(WIN32)
	set(WINDOW_MANAGEMENT_SOURCE ${WINDOW_MANAGEMENT_WINDOWS_SOURCE}) #for now
	set(GRAPHICS_PRIMITIVES_SOURCES ${GRAPHICS_PRIMITIVES_DIRECTX_SOURCES})
	set(RENDERING_CONTEXT_SOURCE ${RENDERING_CONTEXT_DIRECTX_SOURCES})
	set(GRAPHICS_PRIMITIVES_SOURCES ${GRAPHICS_PRIMITIVES_DIRECTX_SOURCES})
	set(3D_LIBS ${DirectX_D3D11_LIBRARY})
else()
	message(FATAL_ERROR "Non-windows not supported yet")
endif()

SOURCE_GROUP("Windowing" FILES ${WINDOW_MANAGEMENT_API})
SOURCE_GROUP("Windowing\\impl" FILES ${WINDOW_MANAGEMENT_SOURCE})
SOURCE_GROUP("Rendering Context" FILES ${RENDERING_CONTEXT_API})
SOURCE_GROUP("Rendering Context\\impl" FILES ${RENDERING_CONTEXT_SOURCE})
SOURCE_GROUP("Platform specific" FILES ${PLATFORM_SPECIFIC_HEADERS})
SOURCE_GROUP("Graphics Primitives" FILES ${GRAPHICS_PRIMITIVES_API})
SOURCE_GROUP("Graphics Primitives\\impl" FILES ${GRAPHICS_PRIMITIVES_SOURCES})

add_library(${PROJECT_NAME} SHARED
	${GRAPHICS_PRIMITIVES_API}
	${WINDOW_MANAGEMENT_API}
	${WINDOW_MANAGEMENT_SOURCE}
	${DXGE_PUBLIC_HEADERS}
	${GRAPHICS_PRIMITIVES_SOURCES}
	${RENDERING_CONTEXT_SOURCE}
)

target_link_libraries(${PROJECT_NAME}
	${3D_LIBS}
)

add_dependencies(${PROJECT_NAME}
	eigen
)

if(WIN32)
	set_target_properties(${PROJECT_NAME} PROPERTIES DEFINE_SYMBOL DXGE_EXPORTS)
endif()
