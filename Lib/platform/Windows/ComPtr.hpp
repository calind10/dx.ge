#ifndef DXGE_WINDOWS_SPECIFIC_COMPTR_HPP
#define DXGE_WINDOWS_SPECIFIC_COMPTR_HPP

#include <type_traits>
#include <Unknwn.h>
#include <assert.h>

namespace win32
{
namespace details
{
	template<typename T>
	using IsCOMType = std::is_base_of < IUnknown, T > ;
}

template<typename T>
class COMPtr
{
public:
	typedef T element_type;
	static_assert(details::IsCOMType<T>::value, "Type does not inherit from IUnknown");

	COMPtr() : m_pCOM(nullptr) { }
	explicit COMPtr(T* ptr, bool incRef = true) 
		: m_pCOMPtr(ptr)
	{
		if (incRef)
			this->addRef();
	}

	COMPtr(const COMPtr<T>& other)
		: m_pCOM(other.m_pCOM)
	{
		this->addRef();
	}

	COMPtr<T>& operator=(const COMPtr<T>& other)
	{

	}

	COMPtr(COMPtr<T>&& rhs)
		: m_pCOM(rhs.m_pCOM)
	{
		rhs.m_pCOM = nullptr;
	}

	COMPtr & operator=(COMPtr<T>&& rhs)
	{
		COMPtr<T>(std::move(rhs)).swap(*this);
		return *this;
	}

	~COMPtr()
	{
		this->release();
	}

	T** getPP()
	{
		return &m_pCOM;
	}

	void swap(COMPtr<T>& other)
	{
		T* tmp = this->m_pCOM;
		this->m_pCOM = other.m_pCOM;
		other.m_pCOM = tmp;
	}

	COMPtr& operator=(T* rhs)
	{
		COMPtr<T>(rhs).swap(*this);
		return *this;
	}

	void reset()
	{
		COMPtr<T>().swap(*this);
	}

	void reset(T* rhs)
	{
		COMPtr<T>(rhs).swap(*this);
	}

	void reset(T* rhs, bool add_ref)
	{
		COMPtr<T>(rhs, add_ref).swap(*this);
	}

	T* get() const
	{
		return m_pCOM;
	}

	T* detach()
	{
		T * ret = m_pCOM;
		m_pCOM = nullptr;
		return ret;
	}

	T* operator->() const
	{
		assert(m_pCOM != nullptr);
		return m_pCOM;
	}

	operator bool() const
	{
		return m_pCOM != nullptr;
	}

private:
	T* m_pCOM;

	void addRef()
	{
		if (this->m_pCOM)
			this->m_pCOM->AddRef();
	}

	void release()
	{
		T* tmp = this->m_pCOM;
		if (tmp)
		{
			this->m_pCOM = nullptr;
			tmp->Release();
		}
	}
};

}

#endif