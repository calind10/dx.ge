set(EIGEN_ROOT ${CMAKE_SOURCE_DIR}/eigen)
set(EIGEN_INSTALL ${EIGEN_ROOT}/INSTALL)

include(ExternalProject)

ExternalProject_Add( eigen
PREFIX ${EIGEN_ROOT}
HG_REPOSITORY "https://bitbucket.org/eigen/eigen/"
HG_TAG ${EigenSubmodule_FIND_VERSION}
CMAKE_ARGS "-DCMAKE_INSTALL_PREFIX=${EIGEN_INSTALL}"
)

set(Eigen_INCLUDE_DIR ${EIGEN_INSTALL}/include/eigen${EigenSubmodule_FIND_VERSION_MAJOR} CACHE PATH "eigen include directory")