set(Boost_DOWNLOAD_PREFIX ${CMAKE_SOURCE_DIR}/boost)
string(REPLACE "." "_" BOOST_DOWNLOAD_VERSION ${BoostSubmodule_FIND_VERSION})
set(BOOST_URL "http://sourceforge.net/projects/boost/files/boost/${BoostSubmodule_FIND_VERSION}/boost_${BOOST_DOWNLOAD_VERSION}.zip/download")

file(DOWNLOAD http://sourceforge.net/projects/boost/files/boost/${BoostSubmodule_FIND_VERSION}/README.md/download
	${Boost_DOWNLOAD_PREFIX}/README.MD
	STATUS MDStatus
)

set(BOOST_NEEDS_TO_DOWNLOAD ON)
set(BOOST_DOWNLOADED_ZIP ${Boost_DOWNLOAD_PREFIX}/boost_${BoostSubmodule_FIND_VERSION}.zip)
if(MDStatus EQUAL "0")
	file(READ ${Boost_DOWNLOAD_PREFIX}/README.MD HASHES)
	string(REGEX MATCH "\\(./boost_${BOOST_DOWNLOAD_VERSION}.zip\\) = ([0-9|a-z]*)" FOUND_MATCH ${HASHES})
	set(READ_HASH ${CMAKE_MATCH_1})
	if(DEFINED READ_HASH AND EXISTS ${BOOST_DOWNLOADED_ZIP})
		file(MD5 ${BOOST_DOWNLOADED_ZIP} DOWNLOAD_HASH)
		if(DOWNLOAD_HASH EQUAL READ_HASH)
			message(STATUS "Boost doesn't need to be downloaded")
			set(BOOST_NEEDS_TO_DOWNLOAD OFF)
		else()
			message(STATUS "Boost needs to be downloaded")
		endif()
	endif()
else()
	message(STATUS "Download status: ${MDStatus}")
endif()

set(BOOST_NEEDS_TO_EXTRACT OFF)
if(BOOST_NEEDS_TO_DOWNLOAD)
	file(DOWNLOAD ${BOOST_URL}
		${BOOST_DOWNLOADED_ZIP}
		STATUS FullStatus
		SHOW_PROGRESS
	)
	list(GET FullStatus 0 DownloadCode)
	if( NOT DownloadCode EQUAL "0")
		message(FATAL_ERROR "Could not download from: ${BOOST_URL}")
	endif()
	set(BOOST_NEEDS_TO_EXTRACT ON)
endif()

if(NOT EXISTS ${BOOST_DOWNLOADED_ZIP})
	message(FATAL_ERROR "Zip not found at: ${BOOST_DOWNLOADED_ZIP}")
endif()

if(BOOST_NEEDS_TO_EXTRACT)
	execute_process(COMMAND ${CMAKE_COMMAND} -E tar -xzf ${BOOST_DOWNLOADED_ZIP}
		WORKING_DIRECTORY ${Boost_DOWNLOAD_PREFIX}
		RESULT_VARIABLE ExtractResult
	)
	if(NOT ExtractResult EQUAL "0")
		message(FATAL_ERROR "Failed extracting ${BOOST_DOWNLOADED_ZIP} to ${BOOST_EXTRACT_DIR}")
	endif()

	if(WIN32)
		set(BOOTSTRAP "bootstrap.bat")
	else()
		set(BOOTSTRAP "bootstrap.sh")
	endif()
	# append compiler path to environment
	get_filename_component(COMPILER_DIR ${CMAKE_C_COMPILER} DIRECTORY)
	list(APPEND CMAKE_PROGRAM_PATH ${COMPILER_DIR})

	set(BOOST_EXTRACT_DIR ${Boost_DOWNLOAD_PREFIX}/boost_${BOOST_DOWNLOAD_VERSION})
	find_file(BOOTSTRAP_PATH NAMES ${BOOTSTRAP} PATHS ${BOOST_EXTRACT_DIR} NO_DEFAULT_PATH)
	execute_process(COMMAND ${BOOTSTRAP_PATH}
		WORKING_DIRECTORY ${BOOST_EXTRACT_DIR}
		RESULT_VARIABLE BootstrapResult
	)
	if(NOT BootstrapResult EQUAL "0")
		message(FATAL_ERROR "Error while running ${BOOTSTRAP_PATH} in ${BOOST_EXTRACT_DIR}")
	endif()
	find_program(b2Path NAMES b2 PATHS ${BOOST_EXTRACT_DIR} NO_DEFAULT_PATH)
	if(NOT b2Path)
		message(FATAL_ERROR "b2 executable not found in ${BOOST_EXTRACT_DIR}")
	endif()
endif()
set(BOOST_INSTALL ${Boost_DOWNLOAD_PREFIX}/install)
if(BOOST_NEEDS_TO_EXTRACT)
	execute_process(COMMAND ${b2Path}
			--prefix=${BOOST_INSTALL}
			install
			headers
			--without-signals
			-j8
		WORKING_DIRECTORY ${BOOST_EXTRACT_DIR}
		RESULT_VARIABLE B2Result
		ERROR_VARIABLE B2Error
	)
	if(NOT B2Result EQUAL "0")
		message(FATAL_ERROR "Error while running ${b2Path}: ${B2Error}")
	endif()
endif()
set(BOOST_ROOT ${BOOST_INSTALL})
find_package(Boost ${BoostSubmodule_FIND_VERSION} REQUIRED)