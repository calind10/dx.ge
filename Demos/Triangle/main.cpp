#include <Windowing/IWindow.h>
#include <Windowing/Application.h>
#include <Rendering Context/RenderingContext.h>
#include <Rendering Context/RenderingTarget.h>
#include <Rendering Context/RenderingContextDescription.h>

int main()
{
	Application app;

	IWindowPtr pWindow = app.createWindow(800, 600, "Hello World");
	pWindow->initializeRenderingContext(RenderingContextDescription());

	while (app.tick())
	{
		auto context = pWindow->getRenderContext();
		auto pTarget = context->getBackRenderTarget(0);
		pTarget->bind();
		pTarget->clearColor({ 0.0f, 0.0f, 0.0f, 1.0f });
		context->presetBackBuffer();
	}

	return 0;
}