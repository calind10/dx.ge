#define BOOST_TEST_MODULE IWindowTest
#include <boost/test/unit_test.hpp>
#include <Windowing/Application.h>
#include <Windowing/IWindow.h>

class WindowFixture
{
public:
	WindowFixture() { }
	~WindowFixture() { }

protected:
	Application m_app;
};

BOOST_FIXTURE_TEST_CASE(Title, WindowFixture)
{
	IWindowPtr pWindow = m_app.createWindow(10, 10, "Title");
	BOOST_REQUIRE(pWindow);
	BOOST_CHECK_EQUAL(pWindow->getTitle(), "Title");

	pWindow->setTitle("Title2");
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK_EQUAL(pWindow->getTitle(), "Title2");

	std::string readTitle;
	boost::signals2::scoped_connection conn = pWindow->connectTitleChanged([&](const std::string& title)
	{
		readTitle = title;
	});
	pWindow->setTitle("Title3");
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK_EQUAL(readTitle, "Title3");
	BOOST_CHECK_EQUAL(pWindow->getTitle(), "Title3");
}

BOOST_FIXTURE_TEST_CASE(Size, WindowFixture)
{
	IWindowPtr pWindow = m_app.createWindow(200, 10, "Title");
	BOOST_REQUIRE(pWindow);
	BOOST_CHECK_EQUAL(pWindow->getSize(), IWindow::size_type( 200, 10 ));

	IWindow::size_type size1(250, 20);
	pWindow->setSize(size1);
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK_EQUAL(pWindow->getSize(), size1);

	IWindow::size_type readSize;
	boost::signals2::scoped_connection conn = pWindow->connectSizeChanged([&](const IWindow::size_type& rSize)
	{
		readSize = rSize;
	});

	IWindow::size_type size2(300, 300);
	pWindow->setSize(size2);
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK_EQUAL(readSize, size2);
	BOOST_CHECK_EQUAL(pWindow->getSize(), size2);
}

BOOST_FIXTURE_TEST_CASE(Position, WindowFixture)
{
	IWindowPtr pWindow = m_app.createWindow(200, 10, "Title");
	BOOST_REQUIRE(pWindow);

	IWindow::position_type position(10, 20);
	pWindow->setPosition(position);
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK_EQUAL(pWindow->getPosition(), position);

	IWindow::position_type readPosition;
	boost::signals2::scoped_connection conn = pWindow->connectPositionChanged([&](const IWindow::position_type& rPos)
	{
		readPosition = rPos;
	});

	IWindow::position_type position2(300, 300);
	pWindow->setPosition(position2);
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK_EQUAL(readPosition, position2);
	BOOST_CHECK_EQUAL(pWindow->getPosition(), position2);
}

BOOST_FIXTURE_TEST_CASE(Close, WindowFixture)
{
	IWindowPtr pWindow = m_app.createWindow(200, 10, "Title");
	BOOST_REQUIRE(pWindow);

	bool closedEventEmitted = false;
	boost::signals2::scoped_connection conn = pWindow->connectClosed([&]()
		{
			closedEventEmitted = true;
		}
	);
	pWindow->close();
	BOOST_CHECK(!m_app.tick());
	BOOST_CHECK(closedEventEmitted);

	//TODO: test calls on a closed window
}

BOOST_FIXTURE_TEST_CASE(WindowVisibility, WindowFixture)
{
	IWindowPtr pWindow = m_app.createWindow(200, 10, "Title");
	BOOST_REQUIRE(pWindow);

	pWindow->showWindow(true);
	BOOST_CHECK(pWindow->isVisible());
	pWindow->showWindow(true);
	BOOST_CHECK(pWindow->isVisible());

	pWindow->showWindow(false);
	BOOST_CHECK(!pWindow->isVisible());
	pWindow->showWindow(false);
	BOOST_CHECK(!pWindow->isVisible());
}

BOOST_FIXTURE_TEST_CASE(MinimizeMaximize, WindowFixture)
{
	IWindowPtr pWindow = m_app.createWindow(200, 10, "Title");
	BOOST_REQUIRE(pWindow);

	bool signalTriggered = false;
	bool hasBeenMinimized = false;
	boost::signals2::scoped_connection conn = pWindow->connectMinimizeChanged([&](bool isMinimized)
	{
		signalTriggered = true;
		hasBeenMinimized = isMinimized;
	});

	pWindow->minimizeWindow(true);
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK(signalTriggered);
	BOOST_CHECK(hasBeenMinimized);
	BOOST_CHECK(pWindow->isMinimized());

	// minimize some more. nothing should change/be triggered
	signalTriggered = false;
	pWindow->minimizeWindow(true);
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK(!signalTriggered);
	BOOST_CHECK(hasBeenMinimized);
	BOOST_CHECK(pWindow->isMinimized());

	signalTriggered = false;
	pWindow->minimizeWindow(false);
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK(signalTriggered);
	BOOST_CHECK(!hasBeenMinimized);
	BOOST_CHECK(!pWindow->isMinimized());

	// maximize some more. nothing should change/be triggered
	signalTriggered = false;
	pWindow->minimizeWindow(false);
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK(!signalTriggered);
	BOOST_CHECK(!hasBeenMinimized);
	BOOST_CHECK(!pWindow->isMinimized());
}

BOOST_FIXTURE_TEST_CASE(Foreground, WindowFixture)
{
	IWindowPtr pWindow = m_app.createWindow(200, 10, "Title");
	BOOST_REQUIRE(pWindow);

	BOOST_CHECK(m_app.tick());
	BOOST_CHECK(pWindow->isForeground());

	bool signalTriggered = false;
	bool hasFocus = true;
	boost::signals2::scoped_connection conn = pWindow->connectFocusChanged([&](bool focus)
	{
		signalTriggered = true;
		hasFocus = focus;
	});

	pWindow->setForeground();
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK(!signalTriggered);
	BOOST_CHECK(hasFocus);
	BOOST_CHECK(pWindow->isForeground());

	IWindowPtr pWindow2 = m_app.createWindow(200, 10, "Title");
	BOOST_REQUIRE(pWindow2);

	BOOST_CHECK(m_app.tick());
	BOOST_CHECK(pWindow2->isForeground());
	BOOST_CHECK(signalTriggered);
	BOOST_CHECK(!hasFocus);
	BOOST_CHECK(!pWindow->isForeground());


	pWindow->setForeground();
	BOOST_CHECK(m_app.tick());
	BOOST_CHECK(!pWindow2->isForeground());
	BOOST_CHECK(signalTriggered);
	BOOST_CHECK(hasFocus);
	BOOST_CHECK(pWindow->isForeground());
}