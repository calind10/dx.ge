#define BOOST_TEST_MODULE RenderingContextTest
#include <boost/test/unit_test.hpp>
#include <Windowing/Application.h>
#include <Windowing/IWindow.h>
#include <Rendering Context/RenderingContext.h>
#include <Rendering Context/RenderingContextDescription.h>

class RenderingContextFixure
{
public:
	RenderingContextFixure()
	{
		m_pWindow = m_app.createWindow(800, 600, "RenderContextTest");
		m_pWindow->initializeRenderingContext(RenderingContextDescription());
	}

protected:
	Application m_app;
	IWindowPtr m_pWindow;
};

BOOST_AUTO_TEST_CASE(creation)
{
	Application app;
	IWindowPtr pWindow = app.createWindow(800, 600, "RenderContextCreation");
	BOOST_REQUIRE(pWindow);
	BOOST_CHECK_NO_THROW(pWindow->initializeRenderingContext(RenderingContextDescription()));
	BOOST_CHECK(pWindow->getRenderContext());
}

BOOST_FIXTURE_TEST_CASE(getBackRenderingTarget, RenderingContextFixure)
{
	auto pContext = m_pWindow->getRenderContext();
	BOOST_REQUIRE(pContext);

	RenderingTargetPtr pTarget = pContext->getBackRenderTarget(0);
	BOOST_CHECK(pTarget);

	BOOST_CHECK_THROW(pContext->getBackRenderTarget(10), std::exception);
}