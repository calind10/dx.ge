#define BOOST_TEST_MODULE ApplicationTest
#include <boost/test/unit_test.hpp>
#include <Windowing/Application.h>
#include <Windowing/IWindow.h>

BOOST_AUTO_TEST_CASE(init)
{
	// create an app
	{
		Application app;
		// try to create second app.
		BOOST_CHECK_THROW(Application(), std::exception);
	}
	// first app should have been destroyed properly so the when third app is created it doesn't throw any exceptions.
	BOOST_CHECK_NO_THROW(
		{
			Application app;
		}
	);
}

BOOST_AUTO_TEST_CASE(createWindow)
{
	Application app;
	IWindowPtr pWindow = app.createWindow(10, 10, "");
	BOOST_REQUIRE(pWindow);
	BOOST_CHECK_EQUAL(pWindow->getTitle(), "");

	IWindowPtr pWindow2 = app.createWindow(10, 10, "Test");
	app.tick();
	BOOST_REQUIRE(pWindow2);
	BOOST_CHECK_EQUAL(pWindow2->getTitle(), "Test");
}

BOOST_AUTO_TEST_CASE(tick)
{
	Application app;
	// app should tick.
	BOOST_CHECK(app.tick());
	IWindowPtr pWindow = app.createWindow(10, 10, "Test");
	// tick many times in a row. that way all OS messages should have been processed and tick should still return true.
	for (size_t i = 0; i < 100; i++)
	{
		BOOST_CHECK(app.tick());
	}
	pWindow->close();
	// all windows closed => application closes
	BOOST_CHECK(!app.tick());
}